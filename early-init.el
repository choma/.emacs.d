;;; early-init.el --- early bird  -*- no-byte-compile: t -*-

;;; Commentary:

;; Customizations to take effect during Emacs startup earlier than the
;; normal init file is processed.  See:
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Early-Init-File.html

;;; Code:

;; If an `.el' file is newer than its corresponding `.elc', load the
;; `.el'.  See: https://github.com/emacscollective/auto-compile
(setq load-prefer-newer t)

;; This is not working for my current version of emacs (1:28.2+1-10).
;; TODO review in future versions
;; Move native compilation cache folder to `var/eln-cache' (default
;; `no-littering' package folder for persistent data). See:
;; https://github.com/emacscollective/no-littering/blob/main/README.org#native-compilation-cache
(when (fboundp 'startup-redirect-eln-cache)
  (startup-redirect-eln-cache
   (convert-standard-filename
    (expand-file-name  "var/eln-cache/" user-emacs-directory))))

;; Make sure lsp-mode uses plists. See:
;; https://emacs-lsp.github.io/lsp-mode/page/performance/#use-plists-for-deserialization
(setenv "LSP_USE_PLISTS" "true")

;; So we can detect this having been loaded
(provide 'early-init)

;;; early-init.el ends here
