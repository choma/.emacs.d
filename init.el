;;; init.el --- -*- lexical-binding: t; no-byte-compile: t; -*-

;;; Commentary:
;;
;; GNU Emacs configuration file, by Matías Croce

;;; Code:

;; (setq debug-on-error t)

;;;; PERSONAL DATA
(setq user-full-name "Matías Croce"
      user-mail-address "mati@nelumboweb.com.ar")


;;;; PACKAGES

;; Install signed code only, see:
;; https://glyph.twistedmatrix.com/2015/11/editor-malware.html
(setq tls-checktrust t
      gnutls-log-level '1)
(let ((trustfile
       (replace-regexp-in-string
        "\\\\" "/"
        (replace-regexp-in-string
         "\n" ""
         (shell-command-to-string "python -m certifi")))))
  (setq tls-program
        (list
         (format "gnutls-cli%s --x509cafile %s -p %%p %%h"
                 (if (eq window-system 'w32) ".exe" "") trustfile)))
  (setq gnutls-verify-error t
        gnutls-trustfiles (list trustfile)))

;; Add package sources
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")
                         ("melpa" . "https://melpa.org/packages/")))
(package-initialize)
(package-refresh-contents)

(unless (package-installed-p 'use-package)
  (progn
    (package-refresh-contents)
    (package-install 'use-package)))
(setq use-package-verbose t
      use-package-always-ensure t ; use :ensure t for all packages
      use-package-always-defer t) ; use :defer t for all packages

(eval-when-compile
  (require 'use-package))

;; Keep ~/.emacs.d directory clean
(use-package no-littering
  ;; no-littering suggested settings. See:
  ;; https://github.com/emacscollective/no-littering/blob/main/README.org#suggested-settings
  :config
  (no-littering-theme-backups))

(setq custom-file (no-littering-expand-etc-file-name "custom.el"))
(load custom-file 'noerror)

(require 'bind-key)

(use-package auto-compile)
(auto-compile-on-load-mode)

;; (use-package quelpa)

(use-package diminish)

(use-package command-log-mode
  :config
  (command-log-mode))

;; Elisp Bug Hunter!!
;; (use-package bug-hunter)


;;;; ENVIRONMENT DATA

;; Make exec-path === PATH, to make sure Emacs finds all executables
(add-hook 'after-init-hook #'exec-path-from-shell-initialize)
(use-package exec-path-from-shell
  :init
  (progn
    ;; Use a non-interactive shell to load faster. See:
    ;; https://github.com/purcell/exec-path-from-shell#setting-up-your-shell-startup-files-correctly
    (setq exec-path-from-shell-arguments nil)
    ;;
    (when (memq window-system '(mac ns x))
      (exec-path-from-shell-initialize))
    ;; https://github.com/purcell/exec-path-from-shell#usage
    (when (daemonp)
      (exec-path-from-shell-initialize)))
  :config
  (dolist (var '("LSP_USE_PLISTS"))
    (add-to-list 'exec-path-from-shell-variables var)))

(require 'iso-transl) ; Make <dead-acute> work again
(setq current-language-environment "Spanish"
      default-input-method "spanish-prefix")

;; Set locale to UTF8
(set-language-environment 'utf-8)
(set-terminal-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)


;;;; EMACS USAGE

;; remap previous-buffer, next-buffer (default keys, <left> and <right>, are too far)
(global-set-key (kbd "C->") 'next-buffer)
(global-set-key (kbd "C-<") 'previous-buffer)

;; tramp
(setq tramp-default-method "ssh"
      tramp-verbose 7
      remote-file-name-inhibit-cache nil
      vc-ignore-dir-regexp (format "%s\\|%s"
                                   vc-ignore-dir-regexp
                                   tramp-file-name-regexp))

(defadvice projectile-project-root (around ignore-remote first activate)
  "Tell Projectile to ignore remotes."
  (unless (file-remote-p default-directory) ad-do-it))


;; https://askubuntu.com/questions/646631/emacs-doesnot-work-with-xdg-open
;; see https://stackoverflow.com/questions/10766550/how-do-i-execute-a-shell-command-in-background
(defun choma-xdg-open ()
  "Open files from Dired with xdg-open.
It uses 'setsid -w' to wait for the child process, avoiding errors."
  (interactive)
  (if (executable-find "xdg-open")
      (let ((file (ignore-errors (dired-get-file-for-visit))))
        (start-process-shell-command "choma-xdg-open" "*Messages*" (concat "setsid -w open " (shell-quote-argument (file-truename file))))
        ;; (dired-do-async-shell-command "open" nil (list (file-truename file)))
        ;; (dired-do-async-shell-command "evince" nil (dired-get-marked-files))
        )
    (message "Command not found: xdg-open"))
  )

(use-package dired
  :ensure nil ; don't try to install it
  :config
  (put 'dired-find-alternate-file 'disabled nil)
  ;; Move files between split panes
  (setq dired-dwim-target 1)
  :bind(:map dired-mode-map
             ("C-o" . 'choma-xdg-open)))

;; Solo es necesario si habilito el código de abo-abo
;; (use-package dired-x
;;   :ensure nil
;;   :after dired)

;; (use-package dired+)
;; ;; Do not open new buffers when visiting new directories
;; (diredp-toggle-find-file-reuse-dir 1)

(use-package dired-k
  :after dired
  :config
  (add-hook 'dired-initial-position-hook 'dired-k)
  (add-hook 'dired-after-readin-hook #'dired-k-no-revert)
  :init
  (require 'dired-k))

(use-package dired-hacks-utils)
(use-package dired-subtree
  :after dired
  :config
  (bind-key "<tab>" #'dired-subtree-toggle dired-mode-map)
  (bind-key "<backtab>" #'dired-subtree-cycle dired-mode-map)
  :init
  (require 'dired-subtree))

;; narrow dired to match filter
(use-package dired-narrow
  :after dired-k
  :bind (:map dired-mode-map
              ("/" . dired-narrow)))

;; (use-package dired-open
;;   :defer t
;;   :after dired-hacks-utils
;;   :bind(:map dired-mode-map
;;	     ("C-o" . dired-open-xdg))
;;   )

(use-package csv-mode
  :hook
  (csv-mode . csv-guess-set-separator)
  (csv-mode . csv-align-mode))
(add-hook 'csv-mode-hook #'(lambda () (interactive) (toggle-truncate-lines t)))
(add-hook 'csv-mode-hook #'(lambda () (interactive) (visual-line-mode nil)))

(use-package pdf-tools
  :init
  (pdf-tools-install))

;; Ask "y" or "n" instead of "yes" or "no"
(fset 'yes-or-no-p 'y-or-n-p)

;; Edit browser text-areas with emacs. Needs GhostText addon to work:
;; https://github.com/fregante/GhostText
(use-package atomic-chrome)
(atomic-chrome-start-server)

;; search matches counter
(use-package anzu
  :diminish)
(global-anzu-mode +1)

;; Magit!! :D
(use-package magit
  :custom
  (magit-log-margin '(t "%Y-%m-%d %H:%M:%S" magit-log-margin-width t 20) "Show commits date, not age")
  (magit-diff-refine-hunk 'all "Show word granularity")
  (magit-blame-echo-style 'headings "Show headings")
  (magit-blame-read-only t "Make magit-blame buffers read-only")
  ;; C-x g	magit-status
  ;; C-c g	magit-dispatch
  ;; C-c f	magit-file-dispatch
  (magit-define-global-key-bindings 'recommended "Use magit recomended keybinding")
  (magit-format-file-function #'magit-format-file-nerd-icons "Use icons to show file type in magit-status"))

(use-package magit-delta
  :hook (magit-mode . magit-delta-mode)
  :config
  ;; Configure delta to disable line-numbers for magit. There must be
  ;; a "magit-delta" feature in .gitconfig for this to work. See:
  ;; https://dandavison.github.io/delta/features-named-groups-of-settings.html
  (unless (member "--features" magit-delta-delta-args)
    (setq magit-delta-delta-args (append magit-delta-delta-args '("--features" "magit-delta")))))

(use-package magit-todos)

(use-package magit-gitflow
  :diminish)
(add-hook 'magit-mode-hook 'turn-on-magit-gitflow)

(use-package git-timemachine)

;; git-gutter
(use-package fringe-helper)
(use-package git-gutter-fringe)
(global-git-gutter-mode +1)
(require 'git-gutter-fringe)
;; (setq git-gutter-fr:side 'right-fringe)

(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)

;; Helm
;; Copied from Sacha Chua:
;; http://pages.sachachua.com/.emacs.d/Sacha.html#unnumbered-14
(use-package helm
  :diminish
  :init (progn
          (setq helm-candidate-number-limit 100
                ;; From https://gist.github.com/antifuchs/9238468
                helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
                helm-input-idle-delay 0.01
                helm-quick-update t
                helm-M-x-requires-pattern nil
                helm-M-x-fuzzy-match t
                helm-M-x-show-short-doc t
                helm-ff-skip-boring-files t
                helm-ff-auto-update-initial-value t
                helm-buffers-fuzzy-matching t
                helm-recentf-fuzzy-match t
                ;; helm-autoresize-mode 1
                helm-ff-file-name-history-use-recentf t)
          (helm-mode))
  :bind (("C-c h" . helm-mini)
         ("C-h a" . helm-apropos)
         ("C-x C-b" . helm-buffers-list)
         ("C-x b" . helm-buffers-list)
         ("C-x r l" .  helm-bookmarks)
         ("C-x C-f" . helm-find-files)
         ("M-y" . helm-show-kill-ring)
         ("M-x" . helm-M-x)
         ("C-x c o" . helm-occur) ; search in current buffer
         ("C-x c s" . helm-swoop) ; search in current buffer
         ("C-x c m" . helm-multi-swoop-all) ; search in all buffers
         ("C-x c SPC" . helm-all-mark-rings)))
(helm-autoresize-mode t)

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p t) ; open helm buffer inside current window, not occupy whole other window
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(use-package helm-ag)
(defun projectile-helm-ag ()
  "Searching with silversearcher-ag inside projects with projectile."
  (interactive)
  (helm-ag (projectile-project-root)))

(use-package helm-swoop)

;; For describing bindings
(use-package helm-descbinds
  :bind (("C-h b" . helm-descbinds)
         ("C-h w" . helm-descbinds)))

(use-package project
  :ensure nil
  :config
  ;; Make sure magit-extras is loaded, so the first time
  ;; `project-switch-project' is called [m] is bound to `magit-status'
  (require 'magit-extras))

(use-package projectile
  :diminish
  :init
  (projectile-mode +1)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map)))
;; (projectile-enable-caching t)

;; Make projectile search locally only for SVN repos. See:
;; https://github.com/bbatsov/projectile/issues/520
(setq projectile-svn-command "find . -type f -not -iwholename '*.svn/*' -print0")

;; Helm-Projectile integration
(use-package helm-projectile)
(setq projectile-completion-system 'helm)
(helm-projectile-on)

(use-package sudo-edit)
(global-set-key (kbd "C-c C-r") 'sudo-edit-current-file)


;;;; DOCUMENTATION

(use-package eldoc
  :diminish
  :init (progn
          (add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
          (add-hook 'lisp-interaction-mode-hook 'eldoc-mode)))

(use-package php-eldoc
  :config (add-hook 'php-mode-hook 'php-eldoc-enable))

(use-package company-php)

(add-hook 'php-mode-hook
          (lambda ()
            (set (make-local-variable 'company-backends)
                 ;; '((php-extras-company company-dabbrev-code) company-capf company-files))))
                 '((company-dabbrev-code) company-capf company-files))))


(use-package css-eldoc
  :init(progn
         (eldoc-mode 1)))
(add-hook 'css-mode-hook 'turn-on-css-eldoc)


;;;; CODE EDITING

;; Please! do not indent with tabs by default
(setq-default indent-tabs-mode nil)

;; Auto pair brackets, quotes, etc
(electric-pair-mode t)

;; Delete selected region when start typing
(delete-selection-mode t)

(use-package expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;; syntax highlighting for all buffers
(global-font-lock-mode t)

;; Visual line (soft-word-wrap)
(global-visual-line-mode t)
;; TODO Do I want it to work this way? (I guess this makes <C-a> and
;; <C-e> moves cursor to the start/end of "visual-line", that doesn't
;; seem to be useful)

;; NOTE: toggle-truncate-lines <C-x x t> will change line wrapping
;; behavior

;; Highlight corresponding parentheses when cursor is on one
(show-paren-mode t)
(setq show-paren-style 'expression)

;; Use cua-selection-mode for rectangles. See:
;; https://irreal.org/blog/?p=1559
(cua-selection-mode t)

(use-package move-dup
  :bind (("M-p"   . move-dup-move-lines-up)
         ("C-M-p" . move-dup-duplicate-up)
         ("M-n"   . move-dup-move-lines-down)
         ("C-M-n" . move-dup-duplicate-down)))

(use-package treesit-fold
  :hook (prog-mode . treesit-fold-indicators-mode)
  :config
  ;; stolen from: https://github.com/mattduck/dotfiles/blob/master/emacs.2023.symlink/init.org#treesit-fold
  (defun md/treesit-fold-global-toggle ()
    "Treesit doesn't provide a global toggle function"
    (interactive)
    ;; There's no builtin function that encapsulates the "is anything folded"
    ;; concept, but it looks like we can pull these overlays within a region
    ;; to achieve it
    (if (treesit-fold--overlays-in 'invisible 'treesit-fold (point-min) (point-max))
        (treesit-fold-open-all)
      (treesit-fold-close-all)))
  :bind (
         ("C-<tab>" . treesit-fold-toggle)
         ("<backtab>" . md/treesit-fold-global-toggle)))

;;;; DEBUGGING
;; TODO I need to install bashdb to be able to use this
;; (use-package realgud)

;; TODO Configure tree-sit
(setq treesit-font-lock-level 4)

(use-package lsp-mode
  :diminish
  :config
  (setq lsp-completion-enable-additional-text-edit nil
        ;; things I don't use and are resource intensive
        lsp-java-format-enabled nil
        ;; lsp-java-import-gradle-enabled nil
        lsp-java-autobuild-enabled nil
        lsp-java-format-on-type-enabled nil
        lsp-log-io nil)
  ;; sonarlint
  (setq lsp-sonarlint-java-enabled t
      lsp-sonarlint-javascript-enabled t
      lsp-sonarlint-typescript-enabled t
      lsp-sonarlint-html-enabled t)
  ;; https://github.com/emacs-lsp/lsp-mode/issues/1672#issuecomment-626277665
  (define-key lsp-mode-map (kbd "C-c C-l") lsp-command-map)
  ;; TODO move this somewhere else. I added here 'cause I need it in typescript too
  ;; Customize indentation
  ;; (setq-default indent-tabs-mode nil) ; set globally. TODO remove this if nothing breaks soon

  :init
  (setq lsp-keymap-prefix "C-c C-l"
        ;; Improve performance. See:
        ;; https://emacs-lsp.github.io/lsp-mode/page/performance/#use-plists-for-deserialization
        lsp-use-plists t ; this MUST be in :init
        gc-cons-threshold 100000000
        read-process-output-max (* 1024 1024 5) ; 5mb
        ;; lsp-idle-delay 1
        lsp-log-io nil ; if set to true can cause a performance hit
        lsp-java-vmargs '("-XX:+UseParallelGC"
                          "-XX:GCTimeRatio=4"
                          "-XX:AdaptiveSizePolicyWeight=90"
                          "-Dsun.zip.disableMemoryMapping=true"
                          "-Xmx1G" ; "-Xmx2G"
                          "-Xms100m"
                          ;; Enable lombok support
                          "-javaagent:/home/choma/.m2/repository/org/projectlombok/lombok/1.18.28/lombok-1.18.28.jar"))
  :hook (
         ;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         ;; (XXX-mode . lsp-deferred)
         (php-mode . lsp-deferred)
         (java-mode . lsp-deferred)
         (java-ts-mode . lsp-deferred)
         (js-mode . lsp-deferred)
         (js-ts-mode . lsp-deferred)
         (tsx-ts-mode . lsp-deferred)
         (typescript-ts-mode . lsp-deferred)
         (sh-mode . lsp-deferred)
         (sh-ts-mode . lsp-deferred)
         (web-mode . lsp-deferred)
         (sql-mode . lsp-deferred)
         ;; To use lsp with js/ts remember to:
         ;; npm i -g typescript-language-server; npm i -g typescript
         ;; See: https://emacs-lsp.github.io/lsp-mode/page/lsp-typescript/
         ;;
         ;; NOTE: parece que el Emacs/LSP detectan cuando un proyecto es angular
         ;; e intenta levantar el LSP server correspondiente, resultando en un
         ;; error ya que no está instalado. Por ahora, pruebo instalando ese
         ;; server:
         ;; npm install -g @angular/language-service@next typescript @angular/language-server
         ;; But it is still generating errors... I should disable it by now? how?
         ;; See:
         ;; - https://emacs.stackexchange.com/questions/69736/disable-lsp-mode-for-derived-mode-of-haskell
         ;; - https://emacs.stackexchange.com/questions/70634/how-to-disable-lsp-clangd-server-if-there-is-no-dominating-compile-commands-json

         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp lsp-deferred)

;; LSP Booster: https://github.com/blahgeek/emacs-lsp-booster
(defun lsp-booster--advice-json-parse (old-fn &rest args)
  "Try to parse bytecode instead of json."
  (or
   (when (equal (following-char) ?#)
     (let ((bytecode (read (current-buffer))))
       (when (byte-code-function-p bytecode)
         (funcall bytecode))))
   (apply old-fn args)))
(advice-add (if (progn (require 'json)
                       (fboundp 'json-parse-buffer))
                'json-parse-buffer
              'json-read)
            :around
            #'lsp-booster--advice-json-parse)

(defun lsp-booster--advice-final-command (old-fn cmd &optional test?)
  "Prepend emacs-lsp-booster command to lsp CMD."
  (let ((orig-result (funcall old-fn cmd test?)))
    (if (and (not test?)                             ;; for check lsp-server-present?
             (not (file-remote-p default-directory)) ;; see lsp-resolve-final-command, it would add extra shell wrapper
             lsp-use-plists
             (not (functionp 'json-rpc-connection))  ;; native json-rpc
             (executable-find "emacs-lsp-booster"))
        (progn
          (message "Using emacs-lsp-booster for %s!" orig-result)
          (cons "emacs-lsp-booster" orig-result))
      orig-result)))
(advice-add 'lsp-resolve-final-command :around #'lsp-booster--advice-final-command)


(use-package lsp-sonarlint)

(setq lsp-disabled-clients '(lsp-semgrep))

(use-package lsp-java
  :config
  ;; Config jdtls to use java 15. See:
  ;; https://github.com/huangfeiyu/articles/blob/master/emacs/lsp-java/setupJava8Project.org
  ;; (setq lsp-java-java-path "/usr/lib/jvm/java-15-openjdk-amd64/bin/java")
  ;; https://github.com/emacs-lsp/lsp-java/issues/249
  ;; https://github.com/emacs-lsp/lsp-java/issues/114
  ;; (add-hook 'java-mode-hook 'lsp)
  (setq lsp-java-configuration-runtimes '[(:name "JavaSE-1.8"
                                                 :path "/usr/lib/jvm/java-8-openjdk-amd64/")
                                          (:name "JavaSE-11"
                                                 :path "/usr/lib/jvm/java-11-openjdk-amd64/")
                                          (:name "JavaSE-15"
                                                 :path "/usr/lib/jvm/java-15-openjdk-amd64/")
                                          (:name "JavaSE-17"
                                                 :default t
                                                 :path "/usr/lib/jvm/java-17-openjdk-amd64/")
                                          ]))

(setq c-default-style
      '((java-mode . "java")))
(setq c-default-style
      '((java-ts-mode . "java")))

(defun my-java-mode-hook ()
  "Java mode custom hook."
  ;; Enable lsp
  (lsp)
  ;; lens and springboot support
  ;; (add-hook 'lsp-mode-hook #'lsp-lens-mode)
  (lsp-lens-mode)
  ;; (add-hook 'java-mode-hook #'lsp-java-boot-lens-mode)
  ;; (lsp-java-boot-lens-mode)

  ;; Customize indentation
  ;; (setq-default indent-tabs-mode nil) ; set globally. TODO remove this if nothing breaks soon
  ;; arguments list -> Indent one basic offset.
  (c-set-offset 'arglist-intro '+)
  ;; arguments closing bracket -> No extra indentation.
  (c-set-offset 'arglist-close 0)
  ;; case in switch statements
  (c-set-offset 'case-label '+) ; Temporary. TODO remove!
  ;; Diminish
  (diminish 'java-mode)
  (diminish 'lsp-lens-mode)
  (diminish 'lsp-java-boot-lens-mode))
(add-hook 'java-mode-hook 'my-java-mode-hook)
(add-hook 'java-ts-mode-hook 'my-java-mode-hook)

(use-package lsp-ui
  :commands lsp-ui-mode)
(use-package helm-lsp
  :commands helm-lsp-workspace-symbol)
(use-package lsp-treemacs
  :commands lsp-treemacs-errors-list)

;; optionally if you want to use debugger
(use-package dap-mode
  :after lsp-mode
  :config (dap-auto-configure-mode))

(use-package dap-java
  :ensure nil ; Don't try to install it, it is included with lsp-java (or dap-mode?)
  :config
  (add-hook 'dap-stopped-hook
            (lambda (arg) (call-interactively #'dap-hydra)))
  (dap-ui-mode t)
  ;; enables mouse hover support
  (dap-tooltip-mode t)
  ;; use tooltips for mouse hover
  ;; if it is not enabled `dap-mode' will use the minibuffer.
  ;; (tooltip-mode t)
  ;; displays floating panel with debug buttons
  (dap-ui-controls-mode 1))
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

(use-package php-mode
  :ensure nil
  :config
  (require 'dap-php)
  (dap-php-setup))

(use-package helm-lsp)

(add-hook 'sql-interactive-mode-hook
          (lambda ()
            (toggle-truncate-lines t)))

(use-package editorconfig
  :diminish
  :config
  (editorconfig-mode 1))

(use-package undo-tree
  :diminish
  :config
  (setq undo-tree-enable-undo-in-region t)
  (setq undo-tree-visualizer-diff t))
(global-undo-tree-mode)

;; Highlight nested parens, brackets, braces a different color at each depth.
(use-package rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(use-package multiple-cursors
  :after hydra
  :init
  (require 'multiple-cursors)
  :config
  (global-set-key
   (kbd "C-c m")
   ;; Hydra for multiple-cursors
   ;; taken from: https://github.com/abo-abo/hydra/wiki/multiple-cursors
   (defhydra hydra-multiple-cursors (:hint nil)
     "
 Up^^             Down^^           Miscellaneous           % 2(mc/num-cursors) cursor%s(if (> (mc/num-cursors) 1) \"s\" \"\")
------------------------------------------------------------------
 [_p_]   Next     [_n_]   Next     [_l_] Edit lines  [_0_] Insert numbers
 [_P_]   Skip     [_N_]   Skip     [_a_] Mark all    [_A_] Insert letters
 [_M-p_] Unmark   [_M-n_] Unmark   [_s_] Search      [_q_] Quit
 [_|_] Align with input CHAR       [Click] Cursor at point"
     ("l" mc/edit-lines :exit t)
     ("a" mc/mark-all-like-this :exit t)
     ("n" mc/mark-next-like-this)
     ("N" mc/skip-to-next-like-this)
     ("M-n" mc/unmark-next-like-this)
     ("p" mc/mark-previous-like-this)
     ("P" mc/skip-to-previous-like-this)
     ("M-p" mc/unmark-previous-like-this)
     ("|" mc/vertical-align)
     ("s" mc/mark-all-in-region-regexp :exit t)
     ("0" mc/insert-numbers :exit t)
     ("A" mc/insert-letters :exit t)
     ("<mouse-1>" mc/add-cursor-on-click)
     ;; Help with click recognition in this hydra
     ("<down-mouse-1>" ignore)
     ("<drag-mouse-1>" ignore)
     ("q" nil))
   )
  )

(use-package hydra)

;; (use-package ace-jump-mode
;;   :bind (("C-c C-j" . ace-jump-mode) ;; Jump to char
;;          ("C-c j" . ace-jump-mode-pop-mark))) ;; Jump back to mark
(defvar-keymap my-jump-map
  :doc "My map to jump to things"
  "t" #'avy-goto-char-timer
  "j" #'avy-goto-char
  "w" #'avy-goto-word-1
  "l" #'avy-goto-line
  "r" #'avy-resume
  "b" #'avy-pop-mark)
(keymap-set global-map "C-c j" my-jump-map)
(which-key-add-keymap-based-replacements global-map
  "C-c j" `("Jump" . ,my-jump-map))

(use-package avy
  :config
  (avy-setup-default) ; binds <C-'> to select isearch candidate with avy
  :custom
  (avy-timeout-seconds 1 "Wait one second after I stopped typing")
  (avy-single-candidate-jump nil "Don't jump immediately if there is only one candidate")
  (avy-all-windows 'all-frames "Show candidates on all windows and all frames"))

(use-package unfill
  :bind ([remap fill-paragraph] . unfill-toggle))

;;;; FILES
(setq make-backup-files t
      backup-by-copying t   ; don't clobber symlinks
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)    ; use versioned backups

(use-package company
  :diminish
  :init (progn
          (setq company-auto-complete nil)
          (global-company-mode t)
          (add-to-list 'company-backends 'company-ispell t)
          (add-to-list 'company-backends 'company-css t)
          ;; (add-to-list 'company-backends 'php-extras-company t)
          ;; (add-to-list 'company-backends 'company-dabbrev t) ;; dabrev must be after other backends to work correctly (?)
          ;; (add-to-list 'company-backends 'company-dabbrev-code t)
          (add-to-list 'company-backends 'company-capf t)
          (add-to-list 'company-backends 'company-files t))
  :bind ( ;; ("C-<tab>" . 'company-complete)
         ("C-ñ" . 'company-complete)
         :map company-active-map
         ("C-n" . 'company-select-next)
         ("C-p" . 'company-select-previous)
         ("C-d" . 'company-show-doc-buffer)
         ("<tab>" . 'company-complete)))

(add-hook 'after-init-hook 'global-company-mode)
;; 0.1 second delay before the pop-up appears
(setq company-idle-delay 0.1
      ;; only one character before auto-completion starts
      company-minimum-prefix-length 1
      company-dabbrev-downcase nil)

(use-package pos-tip
  :config
  (require 'pos-tip))
(use-package company-quickhelp)
(company-quickhelp-mode t)

(use-package flycheck
  :diminish
  :config
  ;; disable jshint since we prefer eslint checking
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(javascript-jshint javascript-standard)))
  ;; use eslint with web-mode
  ;; see: https://github.com/flycheck/flycheck/issues/1384#issuecomment-401452743
  (flycheck-add-mode 'javascript-eslint 'web-mode)
  (add-hook 'sh-mode-hook 'flycheck-mode))

(add-hook 'after-init-hook #'global-flycheck-mode)  ; globally enable flycheck
(use-package flycheck-pos-tip
  :after flycheck
  :config
  (flycheck-pos-tip-mode)
  :init
  (require 'flycheck-pos-tip)
  (setq flycheck-display-errors-function #'flycheck-pos-tip-error-messages))
(use-package helm-flycheck
  :bind("C-c ! h" . helm-flycheck))

;; Make sure the local node_modules/.bin/ can be found (for eslint)
;; https://github.com/codesuki/add-node-modules-path
(use-package add-node-modules-path
  :config
  ;; automatically run the function when web-mode starts
  (eval-after-load 'web-mode
    '(add-hook 'web-mode-hook 'add-node-modules-path)))


;; highlight current word ocurrences
(use-package idle-highlight-mode
  :init (progn
          (add-hook 'prog-mode-hook (lambda () (idle-highlight-mode t)))))
(use-package symbol-overlay
  :bind-keymap ("C-c o" . symbol-overlay-map))

;;;; LANGUAGES

;; (use-package tree-sitter-langs)
;; (setq treesit-extra-load-path "~/Descargas/tmp/emacs-tree-sitter-grammars/libs-linux-x64/dist")

(use-package arduino-mode)
;; (use-package company-arduino)

;; nsi (window installer scripts) files
(use-package nsis-mode)

;; .env files
(use-package dotenv-mode)

;; markdown-mode
(use-package markdown-mode)

;; yaml-mode
(use-package yaml-mode)

;; web-mode (php, js, html, css)
(use-package web-mode
  :config
  (fix-web-mode-indentation)
  (setq web-mode-enable-current-element-highlight t
        web-mode-enable-current-column-highlight t
        web-mode-enable-auto-pairing nil))

(defun fix-web-mode-indentation()
  "Fix web-mode indentation."
  (setq web-mode-block-padding 0
        web-mode-script-padding 0
        web-mode-style-padding 0
        web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-indent-style 2))

(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
(add-to-list 'auto-mode-alist '("\\.blade\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ctp\\'" . web-mode)) ; cake views
(add-to-list 'auto-mode-alist '("\\.scss\\'" . web-mode)) ; sass
(add-to-list 'auto-mode-alist '("\\.ts\\'" . web-mode)) ; ts
(add-hook 'sgml-mode-hook 'web-mode) ; and markup modes
(add-hook 'html-mode-hook 'web-mode)
(add-hook 'css-mode-hook 'web-mode)

;; javascript
(add-to-list 'auto-mode-alist '("\\.js\\'" . js-ts-mode))
(add-to-list 'auto-mode-alist '("\\.ts\\'" . tsx-ts-mode))
(add-to-list 'auto-mode-alist '("\\.cjs\\'" . js-mode))
(add-to-list 'auto-mode-alist '("\\.java\\'" . java-ts-mode))

;; js2 (javascript editing)
(use-package js2-mode
  :config
  ;; Source: https://stackoverflow.com/questions/35162106/how-to-disable-js2-mode-syntax-checking-globally-in-spacemacs
  ;; Turn off js2 mode errors & warnings (we lean on eslint/standard)
  (setq js2-mode-show-parse-errors nil
        js2-mode-show-strict-warnings nil
        ;; Increase highlight-level
        js2-highlight-level 3)
  ;; use js2 as a minor mode (just for js linting)
  (add-hook 'js-mode-hook 'js2-minor-mode))
;; (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
;; (add-to-list 'auto-mode-alist '("\\.ts\\'" . js2-mode))
;; (add-to-list 'auto-mode-alist '("\\.vue\\'" . js2-mode))

;; zen coding
(use-package emmet-mode
  :diminish)
(add-hook 'web-mode-hook 'emmet-mode)
(use-package helm-emmet)

(add-to-list 'company-backends 'company-css t)

;; rainbow (displays strings representing colors with the color they represent)
(use-package rainbow-mode
  :init (progn
          (add-hook 'css-mode-hook (lambda () (rainbow-mode 1)))))

;; php
(use-package php-mode
  :init(progn
         ;; Avoid HTML compatibility. See:
         ;; https://github.com/emacs-php/php-mode#avoid-html-template-compatibility
         (setq php-template-compatibility nil)))
(add-hook 'php-mode-hook 'php-enable-psr2-coding-style)
(setq flycheck-phpcs-standard "PSR2")


;; INI, SERVICE and CONF files
(add-to-list 'auto-mode-alist '("\\.gitconfig$" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.service$" . conf-mode))

;; 80 column indicator
;; (use-package fill-column-indicator)
;; (define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode t)))
;; (setq-default fci-rule-column 80) ;; this line breaks global-visual-line-mode
;; (global-fci-mode t)

;; Remove useless whitespaces and add a new line at the end of the file
(setq-default show-trailing-whitespace t)
;; (setq-default highlight-tabs t)
(setq require-final-newline t
      next-line-add-newlines nil)
(add-hook 'before-save-hook 'whitespace-cleanup)
(add-hook 'before-save-hook (lambda() (delete-trailing-whitespace)))

(setq auto-save-default nil
      ;; Startup buffer
      inhibit-startup-message t
      inhibit-splash-screen t
      initial-scratch-message ";; Scratch buffer\n\n(setq debug-on-error t)\n\n"
      ;; Don't keep message buffers around
      message-kill-buffer-on-exit t
      ;; Kill the whole line
      kill-whole-line t)

;;;; LAYOUT

;; See: https://tony-zorman.com/posts/2022-10-22-emacs-potpourri.html
(setq frame-inhibit-implied-resize t)
(pixel-scroll-precision-mode)

(winner-mode 1)

;; remove useless GUI elements
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

;; visible error notification
(setq visible-bell t)

(tooltip-mode t)

(use-package popup)

(use-package all-the-icons
  :if (display-graphic-p))
;; After install is necessary to run:
;; M-x all-the-icons-install-fonts
;; TODO do this programmatically

(use-package nerd-icons)
;; After install is necessary to run:
;; M-x nerd-icons-install-fonts
;; TODO do this programmatically

(use-package which-key
  :diminish
  :config
  (which-key-mode))

(use-package helpful
  :diminish
  :init
  ;; Note that the built-in `describe-function' includes both functions
  ;; and macros. `helpful-function' is functions only, so we provide
  ;; `helpful-callable' as a drop-in replacement.
  (global-set-key (kbd "C-h f") #'helpful-callable)

  (global-set-key (kbd "C-h v") #'helpful-variable)
  (global-set-key (kbd "C-h k") #'helpful-key)

  ;; Lookup the current symbol at point. C-c C-d is a common keybinding
  ;; for this in lisp modes.
  (global-set-key (kbd "C-c C-d") #'helpful-at-point)

  ;; Look up *F*unctions (excludes macros).
  ;;
  ;; By default, C-h F is bound to `Info-goto-emacs-command-node'. Helpful
  ;; already links to the manual, if a function is referenced there.
  (global-set-key (kbd "C-h F") #'helpful-function)

  ;; Look up *C*ommands.
  ;;
  ;; By default, C-h C is bound to describe `describe-coding-system'. I
  ;; don't find this very useful, but it's frequently useful to only
  ;; look at interactive functions.
  (global-set-key (kbd "C-h C") #'helpful-command))

;; Set font like this to fix emacsclient problem. See:
;; See: https://emacs.stackexchange.com/a/52078
;; Motion -> serif
;; Duo -> ancho variable
;; wide -> well... they are wider
(add-to-list 'default-frame-alist '(font . "JetBrains Mono Thin 9"))

;; line number on left fringe)
(global-display-line-numbers-mode 1)
(add-hook 'treemacs-mode-hook (lambda() (display-line-numbers-mode -1)))

(line-number-mode t)        ; line number on mode line
(column-number-mode t)      ; column number on mode line
(global-hl-line-mode 1)     ; highlight current line

;; transparency
;; (set-frame-parameter (selected-frame) 'alpha 95)

(use-package doom-themes
  :init
  (add-hook 'after-init-hook (lambda () (load-theme 'doom-molokai t)))
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t ; if nil, italics is universally disabled
        doom-molokai-brighter-comments t
        ;; doom-molokai-brighter-modeline t
        ;; doom-molokai-padded-modeline t
        )

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-colors") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))
;; TODO set faces for show-parens-mode, the current red color is awful

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))

(use-package treemacs
  :defer t
  :init
  ;; (with-eval-after-load 'winum
  ;;   (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (treemacs-resize-icons 16))
  :bind
  (:map global-map
        ;; ("M-0"       . treemacs-select-window) ;; this breaks git-gutter-fringe
        ("M-0"       . treemacs)
        ("C-c t t"   . treemacs)
        ("C-c t 1"   . treemacs-delete-other-windows)
        ("C-c t d"   . treemacs-select-directory)
        ("C-c t B"   . treemacs-bookmark)
        ("C-c t C-t" . treemacs-find-file)
        ("C-c t M-t" . treemacs-find-tag)))

(use-package treemacs-projectile
  :after (treemacs projectile))

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once))

(use-package treemacs-magit
  :after (treemacs magit))

;; (use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
;;   :after (treemacs persp-mode) ;;or perspective vs. persp-mode
;;   :config (treemacs-set-scope-type 'Perspectives))

;; (use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
;;   :after (treemacs)
;;   :config (treemacs-set-scope-type 'Tabs))


;; Hide modes in mode line
(diminish 'visual-line-mode)
(diminish 'abbrev-mode)
(global-hi-lock-mode t)
(diminish 'hi-lock-mode)
(diminish 'isearch-mode)
(diminish 'auto-revert-mode)
(diminish 'git-gutter-mode)

;;;; DOCS
;; TODO: move all docs packages together
(use-package zeal-at-point
  :bind ("C-c z" . zeal-at-point))

;; SERVICES
(use-package docker)
(use-package dockerfile-mode)
(use-package docker-compose-mode)

;; (use-package restclient)

;;;; ORG-MODE
(setq org-log-done t) ;; Add timestamp when done
;; key bindings
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
;; config
(setq org-directory "~/proyectos/org"
      org-agenda-files (directory-files-recursively org-directory "")
      org-agenda-archives-mode nil
      org-default-notes-file (concat org-directory "/refile.org")
      org-refile-targets '((org-agenda-files . (:maxlevel . 3)))
      ;; SRC blocks
      ;; TODO see: https://github.com/syl20bnr/spacemacs/issues/13255#issuecomment-592998372
      org-src-preserve-indentation t
      org-edit-src-content-indentation 0
      ;; org-src-tab-acts-natively t
      org-modules '(org-protocol))

;; Indent different levels
(add-hook 'org-mode-hook
          (lambda ()
            (org-indent-mode t))
          t)

(use-package org-download
  :after org
  :config
  (org-download-enable))

(use-package org-mode
  :ensure nil)
;; Encrypt all headings with :crypt: tag. To decrypt use `M-x org-decrypt-entry`
(with-eval-after-load 'org
  (require 'org-crypt)
  (org-crypt-use-before-save-magic)
  (setq org-tags-exclude-from-inheritance (quote ("crypt")))
  ;; My gpg key
  (setq org-crypt-key "909301CD9C6F74B7"))

;; (defun transform-square-brackets-to-round-ones(string-to-transform)
;;   "Take STRING-TO-TRANSFORM and transforms every ocurrence of [ into ( and ] into ), other chars left unchanged."
;;   (concat
;;    (mapcar #'(lambda (c) (if (equal c ?[) ?\( (if (equal c ?]) ?\) c))) string-to-transform))
;;   )

;; (setf org-capture-templates nil)

;; ;; (let ((default-directory org-directory))
;; ;;   (setq org-default-notes-file (expand-file-name "notes.org"))
;; ;;   (setq todo-file-path (expand-file-name "gtd.org"))
;; ;;   (setq journal-file-path (expand-file-name "journal.org"))
;; ;;   (setq today-file-path (expand-file-name "2010.org")))
;; (add-to-list 'org-capture-templates
;;              '("p" "Protocol" entry (file+headline org-default-notes-file "Capture")
;;                ;; "* %^{title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n%?"
;;                ;; "* %^{Title}\n\n  Source: %u, %c\n\n  %i"
;;                ;; "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n%?"
;;                ;; "* %^{Title}\n  Source:  %u,  %x  \n\n  %:initial \n\n  %?"
;;                "* %^{Título}\nFuente: %u, [[%:link]][[%(transform-square-brackets-to-round-ones \"%:description\")]]\n\n#+BEGIN_QUOTE\n%:initial\n#+END_QUOTE\n\n%?"
;;                :greedy nil))
;; (add-to-list 'org-capture-templates
;;              '("L" "Protocol Link" entry (file+headline org-default-notes-file "Capture")
;;                "* %? [[%:link][%(transform-square-brackets-to-round-ones \"%:description\")]]\n"))
;; (add-to-list 'org-capture-templates
;;              '("t" "Todo" entry (file+headline org-default-notes-file "Tasks")
;;                "* TODO %?" :prepend t))

;;;; OTHER TOOLS

;; Plantuml
(use-package plantuml-mode
  :init
  (setq org-plantuml-exec-mode 'plantuml)
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode)))
(with-eval-after-load 'org
  (org-babel-do-load-languages 'org-babel-load-languages
                               (append org-babel-load-languages
                                       '((plantuml . t)
                                         (octave . t)
                                         (dot . t)))))
(add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

(use-package nginx-mode
  :config
  (add-to-list 'auto-mode-alist '("/nginx/sites-\\(?:available\\|enabled\\)/" . nginx-mode)))

;; Enable narrow-to-region
;; (narrow: C-x n n )
;; (widen: C-x n w)
(put 'narrow-to-region 'disabled nil)

;;; init.el ends here
